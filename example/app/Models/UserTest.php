<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserTest extends Authenticatable
{
    use HasFactory;
    protected $table = 'usertest';
    protected $fillable = ['name_user', 'email', 'password', 'position', 'remember_token'];
}
