<?php

namespace App\Repositories;

use App\Models\Flights;

class FlightRepository
{

    protected $post;

    public function __construct(Flights $flight)
    {
        $this->flight = $flight;
    }
    public function create($attributes)
    {
        return $this->flight->create($attributes);
    }

    public function findById($id)
    {
        return $this->flight->where('id', $id)->first();
    }

    public function updatePro($request, $id)
    {
        return $this->flight->where('id', $id)->update($request);
    }

    public function deletePro($id)
    {
        return $this->flight->where('id', $id)->delete();
    }
}