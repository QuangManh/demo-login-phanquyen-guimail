<?php
    namespace App\Repositories;
    use App\Models\Admins;

    class AdminRepository{
        public function __construct(Admins $admin)
        {
            $this->admin = $admin;
        }

        public function addAdmin($request)
        {
            return $this->admin->create($request);
        }
    }
?>