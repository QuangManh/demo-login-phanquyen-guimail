<?php
    namespace App\Services;
    use App\Repositories\AdminRepository;
    class AdminService{
        public function __construct(AdminRepository $adminRepository)
        {
            $this->adminrepository = $adminRepository;
        }
        public function addadmin($request)
        {
            return $this->adminrepository->addAdmin($request);
        }
    }
?>