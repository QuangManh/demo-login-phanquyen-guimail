<?php
    namespace App\Services;
    use App\Repositories\UserTestRepository;

    class UserTestService{
        public function __construct(UserTestRepository $userTestRepository)
        {
            $this->usertestrepository = $userTestRepository;
        }
        public function add($request)
        {
            return $this->usertestrepository->addUser($request);
        }
    }
?>